# Docker Image which is used as foundation to create
# a custom Docker Image with this Dockerfile
FROM node

# A directory within the virtualized Docker environment
# Becomes more relevant when using Docker Compose later
WORKDIR /usr/src/app

# Copies package.json and package-lock.json to Docker environment
COPY package*.json ./

# Installs all node packages
RUN npm install

# Copies everything over to Docker environment
COPY . .

# Uses port which is used by the actual application
EXPOSE 3000

# Finally runs the application
CMD [ "npm", "start" ]








# # Stage 0 - Build Frontend Assets
# # FROM node:12.16.3-alpine as build

# FROM node:alpine

# WORKDIR /app
# COPY package*.json ./
# RUN npm install
# COPY . .

# CMD ["npm", "start"]
# # RUN npm run build

# # # Stage 1 - Serve Frontend Assets
# # FROM fholzer/nginx-brotli:v1.12.2

# # WORKDIR /etc/nginx
# # ADD nginx.conf /etc/nginx/nginx.conf

# # COPY --from=build /app/build /usr/share/nginx/html
# # EXPOSE 443
# # CMD ["nginx", "-g", "daemon off;"]
// import React, { useState, useEffect, Fragment } from "react";
// import firebase from "./firebase";
// import React, { Fragment } from "react";
// import GetFirebase from './GetFirebase';
// import SnapshotFirebase from "./SnapshotFirebase";

import React from 'react';
// import GetFirebase from './GetFirebase';
// import SnapshotFirebase from './SnapshotFirebase';
import { AuthProvider } from './auth/Auth';
import Login from './auth/Login';
import Welcome from './Welcome';
import SnapshotFirebaseAdvanced from './SnapshotFirebaseAdvanced';

function App() {
  const get = false;

  return (
    <>
      <AuthProvider>
        <Welcome />
        <Login />
        {/* {get ? <GetFirebase /> : <SnapshotFirebase />} */}
        <SnapshotFirebaseAdvanced />
      </AuthProvider>
    </>
  );
}

export default App;






//////  GetFirebase and SnapshotFirebase firing from 
//////  seperate components
//////  replaced by Auth setup (check above)



// function App() {
//   const get = false;

//   return (
//   <Fragment>
//     {get ? <GetFirebase /> : <SnapshotFirebase />}
//   </Fragment>
//   )
// }

// export default App;

//////  get and snapsot firebase firing from app.js directly
///// replaced by subsequent components

// function App() {
//   const [schools, setSchools] = useState([]);
//   const [loading, setLoading] = useState(false); 
//   const ref = firebase.firestore().collection("schools");
//   function getSchools() {
//     setLoading(true);
//     ref.onSnapshot((querySnapshot) => {
//       const items = [];
//       querySnapshot.forEach((doc) => {
//         items.push(doc.data());
//       });
//       setSchools(items);
//       setLoading(false);     
//     });
//   }
//   function getSchools2() {
//     setLoading(true);
//     ref.get().then((item) => {
//       const items = item.docs.map((doc) => doc.data());
//       setSchools(items);
//       setLoading(false);
//     });    
//   }
//   useEffect(() => {
//     // getSchools();
//     getSchools2();
//   }, []);
//   if (loading) {
//     return <h1>Loading...</h1>;
//   }
//   return (
//     <div>
//       <h1>Schools</h1>
//       {schools.map((school) => (
//         <div key={school.id}>
//           <h2>{school.title}</h2>
//           <p>{school.desc}</p>
//         </div>
//       ))}
//     </div>
//   );
// }
// export default App;